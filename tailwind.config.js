import stylesColor from "./src/common/styles/styles.colors.ts";

/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./app/**/*.{js,ts,jsx,tsx}",
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",

    // Or if using `src` directory:
    "./src/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      spacing: {
        five: "18%",
        three: "32%",
      },
      colors: {
        ...stylesColor,
      },
      fontSize: {
        "title-2": [
          "32px",
          {
            lineHeight: "24px",
          },
        ],
      },
      minWidth: {
        90: "90%",
        40: "40%",
      },
    },
  },
  plugins: [],
};
