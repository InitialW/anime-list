import {
  ApolloClient,
  ApolloProvider,
  InMemoryCache,
  NormalizedCacheObject,
} from "@apollo/client";
import * as React from "react";
import { useNetworkStatus } from "./network-status-container";
import { CachePersistor, LocalStorageWrapper } from "apollo3-cache-persist";

interface Props {
  children: React.ReactNode;
}

export default function ApolloContainer(props: Props) {
  const [client, setClient] =
    React.useState<ApolloClient<NormalizedCacheObject>>();
  const [persistor, setPersistor] =
    React.useState<CachePersistor<NormalizedCacheObject>>();

  React.useEffect(() => {
    async function init() {
      const cache = new InMemoryCache();
      let newPersistor = new CachePersistor({
        cache,
        storage: new LocalStorageWrapper(window.localStorage),
        debug: true,
        trigger: "write",
      });
      await newPersistor.restore();
      setPersistor(newPersistor);
      setClient(
        new ApolloClient({
          uri: "https://graphql.anilist.co",
          cache,
          defaultOptions: {
            watchQuery: {
              fetchPolicy: "cache-and-network",
            },
          },
        })
      );
    }

    init().catch(console.error);
  }, []);

  if (!client) {
    return null;
  }

  return <ApolloProvider client={client}>{props.children}</ApolloProvider>;
}
