import * as React from "react";
import invariant from "invariant";

interface Props {
  children: React.ReactNode;
}

interface NetworkHandleProps {
  online: boolean;
  children: React.ReactNode;
}

export interface NetworkStateProps {
  online: boolean;
}

export const NetworkContext = React.createContext<NetworkStateProps>({
  online: false,
});

export default function NetworkStatusContainer(props: Props) {
  const [networkState, setNetworkState] = React.useState(false);

  const value = React.useMemo<NetworkStateProps>(
    () => ({
      online: networkState,
    }),
    [networkState]
  );

  React.useEffect(() => {
    if (typeof window !== "undefined") {
      setNetworkState(window.navigator.onLine);
    }
  }, []);

  React.useEffect(() => {
    function exec() {
      setNetworkState(true);
    }

    window.addEventListener("online", exec);

    return () => window.removeEventListener("online", exec);
  }, []);

  React.useEffect(() => {
    function exec() {
      setNetworkState(false);
    }

    window.addEventListener("offline", exec);

    return () => window.removeEventListener("offline", exec);
  }, []);

  return (
    <NetworkContext.Provider value={value}>
      {props.children}
    </NetworkContext.Provider>
  );
}

export function useNetworkStatus() {
  const context = React.useContext(NetworkContext);

  invariant(
    context !== undefined,
    "useNetworkStatus must be used inside Credential Container"
  );

  return context;
}
