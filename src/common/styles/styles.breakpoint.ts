// eslint-disable-next-line import/no-anonymous-default-export
export default {
  xs: "414px",
  sm: "576px",
  md: "768px",
  lg: "992px",
  xl: "1280px",
  xxl: "1366px",
};
