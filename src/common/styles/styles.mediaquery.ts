import stylesBreakpoint from "./styles.breakpoint";

// eslint-disable-next-line import/no-anonymous-default-export
export default {
  xs: `@media (min-width: calc(${stylesBreakpoint.xs}))`,
  sm: `@media (min-width: calc(${stylesBreakpoint.sm}))`,
  md: `@media (min-width: calc(${stylesBreakpoint.md}))`,
  lg: `@media (min-width: calc(${stylesBreakpoint.lg}))`,
  xl: `@media (min-width: calc(${stylesBreakpoint.xl}))`,
  xxl: `@media (min-width: calc(${stylesBreakpoint.xxl}))`,
  maxXs: `@media (max-width: calc(${stylesBreakpoint.xs} - 1px))`,
  maxSm: `@media (max-width: calc(${stylesBreakpoint.sm} - 1px))`,
  maxMd: `@media (max-width: calc(${stylesBreakpoint.md} - 1px))`,
  maxLg: `@media (max-width: calc(${stylesBreakpoint.lg} - 1px))`,
  maxXl: `@media (max-width: calc(${stylesBreakpoint.xl} - 1px))`,
  maxXxl: `@media (max-width: calc(${stylesBreakpoint.xxl} - 1px))`,
};
