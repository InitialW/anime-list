import colors from "./styles.colors";
import mediaQuery from "./styles.mediaquery";
import typography, { fontFamily } from "./styles.typography";
import { DEFAULT_THEME, MantineTheme, useMantineTheme } from "@mantine/core";

export const primitives = {
  primaryFontFamily: fontFamily,
};

interface CustomThemeInterface {
  colors: typeof colors;
  typography: typeof typography;
  mediaQuery: typeof mediaQuery;
}

const customThemeValue: CustomThemeInterface = {
  colors,
  typography,
  mediaQuery,
};

export interface CustomTheme extends Omit<MantineTheme, "other"> {
  other: CustomThemeInterface;
}

const CustomThemes: CustomTheme = {
  ...DEFAULT_THEME,
  fontFamily,
  other: {
    colors: {
      ...customThemeValue.colors,
    },
    typography: {
      ...customThemeValue.typography,
    },
    mediaQuery: {
      ...customThemeValue.mediaQuery,
    },
  },
};

export const ANIME_THEME = CustomThemes;
export const useCustomTheme: () => CustomTheme =
  useMantineTheme as () => CustomTheme;
