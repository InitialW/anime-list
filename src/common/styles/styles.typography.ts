export const fontFamily = "Inter";
const typography = {
  title1Bold: {
    fontFamily,
    fontSize: "48px",
    fontWeight: 700,
    lineHeight: 1,
  },
  title1Semibold: {
    fontFamily,
    fontSize: "48px",
    fontWeight: 600,
    lineHeight: 1,
  },

  title2Bold: {
    fontFamily,
    fontSize: "32px",
    fontWeight: 700,
    lineHeight: 24,
  },
  title2Semibold: {
    fontFamily,
    fontSize: "32px",
    fontWeight: 600,
    lineHeight: 24,
  },

  title3Bold: {
    fontFamily,
    fontSize: "24px",
    fontWeight: 700,
    lineHeight: "31px",
  },
  title3Semibold: {
    fontFamily,
    fontSize: "24px",
    fontWeight: 600,
    lineHeight: "31px",
  },

  body1Bold: {
    fontFamily,
    fontSize: "18px",
    fontWeight: 700,
    lineHeight: "28px",
  },
  body1Semibold: {
    fontFamily,
    fontSize: "18px",
    fontWeight: 600,
    lineHeight: "28px",
  },
  body1Regular: {
    fontFamily,
    fontSize: "18px",
    fontWeight: 400,
    lineHeight: "28px",
  },
  body1Light: {
    fontFamily,
    fontSize: "18px",
    fontWeight: 300,
    lineHeight: "23px",
  },

  body2Bold: {
    fontFamily,
    fontSize: "16px",
    fontWeight: 700,
    lineHeight: "21px",
  },
  body2Semibold: {
    fontFamily,
    fontSize: "16px",
    fontWeight: 600,
    lineHeight: "24px",
  },
  body2Regular: {
    fontFamily,
    fontSize: "16px",
    fontWeight: 400,
    lineHeight: "24px",
  },
  body2Light: {
    fontFamily,
    fontSize: "16px",
    fontWeight: 300,
    lineHeight: "24px",
  },

  body3Bold: {
    fontFamily,
    fontSize: "14px",
    fontWeight: 700,
    lineHeight: "18px",
  },
  body3Semibold: {
    fontFamily,
    fontSize: "14px",
    fontWeight: 600,
    lineHeight: "20px",
  },
  body3Regular: {
    fontFamily,
    fontSize: "14px",
    fontWeight: 400,
    lineHeight: "20px",
  },
  body3Light: {
    fontFamily,
    fontSize: "14px",
    fontWeight: 300,
    lineHeight: "20px",
  },

  captionBold: {
    fontFamily,
    fontSize: "12px",
    fontWeight: 700,
    lineHeight: "16px",
  },
  captionSemibold: {
    fontFamily,
    fontSize: "12px",
    fontWeight: 600,
    lineHeight: "16px",
  },
  captionRegular: {
    fontFamily,
    fontSize: "12px",
    fontWeight: 400,
    lineHeight: "16px",
  },
  captionLight: {
    fontFamily,
    fontSize: "12px",
    fontWeight: 300,
    lineHeight: "16px",
  },
};

export default typography;
