// eslint-disable-next-line import/no-anonymous-default-export
export default {
  primaryDark: "#161142",
  primaryDefault: "#2B2277",
  primaryLight: "#626783",
  primaryLighter: "#2B2277",
  primaryLightest: "#2B2277",

  // secondaryDark: "#24BC7C",
  // secondaryDefault: "#38D995",
  // secondaryLight: "#65E2AD",
  // secondaryLighter: "#8FEAC4",
  // secondaryLightest: "#E5FAF1",

  tableBackground: "#DDDDDD",

  redDark: "#B3221E",
  redDefault: "#DB2D28",
  redLight: "#E25955",
  redLighter: "#E98481",
  redLightest: "#F8D9D8",

  blueDark: "#0071CC",
  blueDefault: "#008DFF",
  blueLight: "#33A4FF",
  blueLighter: "#66BBFF",
  blueLightest: "#CCE8FF",

  greenDark: "#39984E",
  greenDefault: "#49BB62",
  greenLight: "#6EC982",
  greenLighter: "#93D7A2",
  greenLightest: "#D6FBD0",

  yellowDark: "#C29900",
  yellowDefault: "#F4C100",
  yellowLight: "#FFD229",
  yellowLighter: "#FFDD5C",
  yellowLightest: "#FFF2C2",

  inkDark: "#050505",
  inkDefault: "#1E1E1E",
  inkLight: "#383838",
  inkLighter: "#525252",
  inkLightest: "#6B6B6B",

  skyDarkest: "#707475",
  skyDark: "#A8A8A8",
  skyDefault: "#C2C2C2",
  skyLight: "#DBDBDB",
  skyLighter: "#F6F6F6",
  skyLightest: "#FFFFFF",
};
