export const resetStyle = `
h1, h2, h3, h4, h5, h6 {
  margin: 0;
}
html {
  font-family: Open Sans, Segoe UI, 'Segoe UI Web (West European)', -apple-system, BlinkMacSystemFont, Helvetica Neue, sans-serif;
  font-size: 16px;
  clear: both;
  box-shadow: none;
}
#__next > div:first-of-type {
  width: 100%;
  height: 100%;
}
* {
  box-sizing: border-box;
  scrollbar-width: thin;
  scrollbar-color: darkgrey transparent;
}
*::-webkit-scrollbar {
  width: 4px;
  height: 4px;
}
*::-webkit-scrollbar-track {
  background: none;
  border-radius: 50%;
}
*::-webkit-scrollbar-thumb {
  background-color: darkgrey;
  border-radius: 24px;
}

.no-scrollbar::-webkit-scrollbar{
  width: 0;
  height: 0;
}

*::before {
  box-sizing: border-box;
}
*::after {
  box-sizing: border-box;
}
ul[class] {
  margin: 0;
  list-style: none;
}
ol[class] {
  margin: 0;
  list-style: none;
}
body {
  max-width: 100vw;
  min-height: 100vh;
  scroll-behavior: smooth;
  text-rendering: optimizeSpeed;
  line-height: initial;
  margin: 0;
}
body > div#__next {
  height: 100vh;
}
figure {
  margin: 0;
}
blockquote {
  margin: 0;
}
dl {
  margin: 0;
}
dd {
  margin: 0;
}
a:not([class]) {
  text-decoration-skip-ink: auto;
}
img {
  max-width: 100%;
  display: block;
}
input {
  font: inherit;
}
button {
  font: inherit;
}
textarea {
  font: inherit;
}
select {
  font: inherit;
}
@media (prefers-reduced-motion: reduce) {
  * {
    animation-duration: 0.01ms !important;
    animation-iteration-count: 1 !important;
    transition-duration: 0.01ms !important;
    scroll-behavior: auto !important;
  }
}
`;
