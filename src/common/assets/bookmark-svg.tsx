import * as React from "react";
import { SVGProps } from "react";

const SvgComponent = (props: SVGProps<SVGSVGElement>) => {
  const { width = 36, height = 36, ...restProps } = props;

  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={width}
      height={height}
      fill="none"
      viewBox="0 0 33 33"
      {...restProps}
    >
      <g clipPath="url(#a)">
        <path
          fill="#000"
          d="M23.912 4.507h-14a2 2 0 0 0-2 2v22a1 1 0 0 0 1.53.847l7.47-4.668 7.471 4.668a.999.999 0 0 0 1.53-.847v-22a2 2 0 0 0-2-2Z"
        />
      </g>
      <defs>
        <clipPath id="a">
          <path fill="#fff" d="M.912.507h32v32h-32z" />
        </clipPath>
      </defs>
    </svg>
  );
};

export default SvgComponent;
