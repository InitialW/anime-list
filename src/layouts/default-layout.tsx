import ApolloContainer from "common/containers/apollo-container";
import NetworkStatusContainer from "common/containers/network-status-container";
import { Header } from "components/widgets";
import * as React from "react";

interface DefaultLayoutProps {
  children?: React.ReactNode;
}

export default function DefaultLayout(props: DefaultLayoutProps) {
  const { children } = props;

  return (
    <div className="flex flex-col w-screen h-screen">
      <ApolloContainer>
        <NetworkStatusContainer>
          <Header />
          <div
            style={{
              height: `calc(100% - 80px)`,
              marginTop: 80,
            }}
          >
            {children}
          </div>
        </NetworkStatusContainer>
      </ApolloContainer>
    </div>
  );
}
