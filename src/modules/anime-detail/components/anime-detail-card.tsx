import * as React from "react";
import { Media } from "_graphql/types";
import * as AspectRatio from "@radix-ui/react-aspect-ratio";
import { thousandSeparatorFormat } from "utils/string";
import { BookmarkOutlineSVG, BookmarkSVG } from "common/assets";
import { getBookmarkCache, updateBookmarkCache } from "../../../utils/bookmark";

interface Props extends Media {}

export default function AnimeDetailCard(props: Props) {
  const [refreshIndex, setRefreshIndex] = React.useState(0);
  const { coverImage, title, genres, popularity, averageScore, description } =
    props;

  let bookmarked = React.useMemo(() => {
    const bookmarks = getBookmarkCache();
    return !!bookmarks?.find((bookmark) => bookmark.id === props.id);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.id, refreshIndex]);

  return (
    <div className="flex flex-col lg:flex-row rounded mb-6 overflow-hidden w-5/5 lg:w-3/4">
      <div className="min-w-90 lg:min-w-40">
        <AspectRatio.AspectRatio ratio={1}>
          <img
            className="w-full h-full"
            alt="anime-img"
            src={coverImage.extraLarge}
          />
        </AspectRatio.AspectRatio>
      </div>
      <div className="py-6 lg:px-8 lg:py-0">
        <div className="flex justify-between">
          <p className="mb-2 typo-body-1-semibold">{`${
            title?.english || "No Title Provided"
          }`}</p>
          <div
            onClick={() => {
              updateBookmarkCache(props);
              setRefreshIndex((prev) => prev + 1);
            }}
            className="cursor-pointer"
          >
            {bookmarked ? <BookmarkSVG /> : <BookmarkOutlineSVG />}
          </div>
        </div>
        <p className="mb-2 typo-body-3-semibold">{`Genre: ${
          genres?.map((_genre) => _genre)?.join(", ") || "No Genre Specified"
        }`}</p>
        <p className="typo-body-3-semibold mb-2">{`Popularity: ${
          thousandSeparatorFormat(popularity) || "0"
        }`}</p>
        <p className="typo-body-3-semibold mb-4">{`Average Score: ${
          thousandSeparatorFormat(averageScore) || "0"
        }`}</p>
        <span className="typo-body-3-regular">
          <div dangerouslySetInnerHTML={{ __html: description }} />
        </span>
      </div>
    </div>
  );
}
