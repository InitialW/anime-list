import { useGetAnime } from "_graphql/queries/anime.query";
import FetchWrapperComponent from "components/fetch-wrapper-component";
import { useRouter } from "next/router";
import * as React from "react";
import AnimeDetailCard from "./components/anime-detail-card";
import { useNetworkStatus } from "common/containers";

export default function AnimeDetail() {
  const router = useRouter();
  const idString = router?.query?.id;

  const id = JSON.parse((idString as string) || "0");

  const { online } = useNetworkStatus();

  const { data, loading, error, refetch } = useGetAnime({
    variables: {
      id,
    },
    fetchPolicy: online ? "cache-and-network" : "cache-only",
  });

  return (
    <div className="w-screen h-screen p-5">
      <div className="flex flex-col items-center">
        <FetchWrapperComponent
          isLoading={loading}
          error={!!error}
          loadingComponent={<>Loading...</>}
          onRetry={() => refetch()}
          component={<>{data?.Media && <AnimeDetailCard {...data?.Media} />}</>}
        />
      </div>
    </div>
  );
}
