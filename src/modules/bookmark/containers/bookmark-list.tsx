import * as React from "react";
import { Media } from "_graphql/types";
import FetchWrapperComponent from "components/fetch-wrapper-component";
import { useRouter } from "next/router";
import AnimeCard from "components/shared/components/anime-card";

interface Props {
  data?: Media[];
}

export default function BookmarkList(props: Props) {
  const { data } = props;
  const router = useRouter();

  const _onNavigateAnimeDetail = React.useCallback(
    (id: string) => {
      router.push(`/anime/${id}`);
    },
    [router]
  );

  return (
    <>
      <div className="flex flex-wrap justify-between">
        <FetchWrapperComponent
          emptyText="No Data"
          noData={!data?.length}
          component={
            <>
              {data?.map((_media) => (
                <AnimeCard
                  key={_media.id}
                  {..._media}
                  onClick={() => _onNavigateAnimeDetail(_media.id)}
                />
              ))}
            </>
          }
        />
        {!!data?.length &&
          Array(5 - (data?.length % 5 || 5))
            .fill({})
            ?.map((_, idx) => (
              <div
                key={`hidden-web-div-${idx}`}
                className="w-five hidden lg:block"
              />
            ))}
        {!!data?.length &&
          Array(3 - (data?.length % 3 || 3))
            .fill({})
            ?.map((_, idx) => (
              <div
                key={`hidden-mobile-div-${idx}`}
                className="w-three lg:hidden"
              />
            ))}
      </div>
    </>
  );
}
