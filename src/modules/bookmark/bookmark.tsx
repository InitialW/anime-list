import * as React from "react";
import BookmarkList from "./containers/bookmark-list";
import { getBookmarkCache } from "utils/bookmark";
import { Media } from "_graphql/types";

export default function Bookmark() {
  const [bookmarks, setBookmarks] = React.useState<Media[]>([]);

  React.useEffect(() => {
    const data = getBookmarkCache();

    setBookmarks(data);
  }, []);

  return (
    <div className="p-5">
      <BookmarkList data={bookmarks || []} />
    </div>
  );
}
