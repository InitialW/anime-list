import { Button } from "components/elements";
import * as React from "react";
import { GenreModalProps, GenreProps } from "./types";
import { Badge, Checkbox, Group, Modal } from "@mantine/core";
import { capitalizeWords } from "utils/string";

const Genre = [
  "action",
  "mystery",
  "drama",
  "romance",
  "ecchi",
  "comedy",
  "fantasy",
  "slice of life",
  "psychological",
  "thriller",
  "sci-fi",
  "adventure",
  "sports",
];

function GenreFilterModal(props: GenreModalProps) {
  const { isOpen, setIsOpen, onApply, value, setValue } = props;
  const [filterValue, setFilterValue] = React.useState<string[]>(value);

  React.useEffect(() => {
    setFilterValue(value);
  }, [value]);

  const _onApply = React.useCallback(() => {
    setValue(filterValue);
    setIsOpen(false);
    onApply?.();
  }, [filterValue, onApply, setIsOpen, setValue]);

  return (
    <Modal
      opened={!!isOpen}
      onClose={() => setIsOpen(false)}
      title="Filter Genre"
      centered
    >
      <Checkbox.Group value={filterValue} onChange={setFilterValue}>
        <Group mt="xs">
          {Genre?.map((_genre) => (
            <Checkbox
              key={_genre}
              value={_genre}
              label={capitalizeWords(_genre)}
            />
          ))}
        </Group>
      </Checkbox.Group>
      <div className="flex w-full flex-1 mt-12 justify-end">
        <Button onClick={_onApply}>Apply</Button>
      </div>
    </Modal>
  );
}

export default function GenreFilter(props: GenreProps) {
  const [isModalOpen, setIsModalOpen] = React.useState(false);

  const { value, setValue, onApply } = props;

  const _styles = {
    root: {
      height: 30,
      minWidth: 200,
      fontSize: 14,
      padding: "4px 16px",
      cursor: "pointer",
    },
    inner: {
      display: "flex",
      flex: 1,
      justifyContent: "center",
      userSelect: "none",
    } as any,
    rightSection: {
      fontSize: 12,
      marginLeft: 12,
    },
  };

  return (
    <div className="mb-8">
      {value?.length ? (
        <Badge
          variant="outline"
          onClick={() => setIsModalOpen(true)}
          styles={_styles}
          rightSection={
            <div
              onClick={(e) => {
                setValue([]);
                onApply?.();
                e.stopPropagation();
                setIsModalOpen(false);
              }}
              className="cursor-pointer"
            >
              X
            </div>
          }
        >
          {value?.map((_) => _).join(", ")}
        </Badge>
      ) : (
        <Button onClick={() => setIsModalOpen(true)} className="w-48">
          Filter Genre
        </Button>
      )}
      <GenreFilterModal
        {...props}
        isOpen={isModalOpen}
        setIsOpen={setIsModalOpen}
      />
    </div>
  );
}
