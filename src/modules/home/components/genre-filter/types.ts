export interface GenreProps {
  value: string[];
  setValue: React.Dispatch<React.SetStateAction<string[]>>;
  onApply?: () => void;
}

export interface GenreModalProps extends GenreProps {
  isOpen: boolean;
  setIsOpen: React.Dispatch<React.SetStateAction<boolean>>;
}
