import * as React from "react";
import { Media } from "_graphql/types";
import FetchWrapperComponent from "components/fetch-wrapper-component";
import AnimeCard from "../../../components/shared/components/anime-card";
import { useRouter } from "next/router";

interface Props {
  refetch: () => void;
  data?: Media[];
  isLoading?: boolean;
  error?: boolean;
}

export default function AnimeList(props: Props) {
  const { data, isLoading, refetch, error } = props;
  const router = useRouter();

  const _onNavigateAnimeDetail = React.useCallback(
    (id: string) => {
      router.push(`/anime/${id}`);
    },
    [router]
  );

  return (
    <>
      <div className="flex flex-wrap justify-between">
        <FetchWrapperComponent
          isLoading={isLoading && !data?.length}
          loadingComponent={<p className="w-full text-center">Loading...</p>}
          error={!!error}
          onRetry={() => refetch()}
          emptyText="No Data"
          noData={!data?.length}
          component={
            <>
              {data?.map((_media) => (
                <AnimeCard
                  key={_media.id}
                  {..._media}
                  onClick={() => _onNavigateAnimeDetail(_media.id)}
                />
              ))}
            </>
          }
        />
        {!!data?.length &&
          Array(5 - (data?.length % 5 || 5))
            .fill({})
            ?.map((_, idx) => (
              <div
                key={`hidden-web-div-${idx}`}
                className="w-five hidden lg:block"
              />
            ))}
        {!!data?.length &&
          Array(3 - (data?.length % 3 || 3))
            .fill({})
            ?.map((_, idx) => (
              <div
                key={`hidden-mobile-div-${idx}`}
                className="w-three lg:hidden"
              />
            ))}
      </div>
      {isLoading && !!data?.length && (
        <p className="w-full text-center">Loading...</p>
      )}
    </>
  );
}
