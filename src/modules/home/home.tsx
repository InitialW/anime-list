import * as React from "react";
import { useGetAnimes } from "_graphql/queries/anime.query";
import { useInView } from "react-intersection-observer";
import { Media } from "_graphql/types";
import AnimeList from "./containers/anime-list";
import { GenreFilter } from "./components/genre-filter";
import { useNetworkStatus } from "common/containers";

const perPage = 10;

export default function Home() {
  const [filterGenres, setFilterGenres] = React.useState<string[]>([]);
  const [page, setPage] = React.useState<number>(1);
  const [allData, setAllData] = React.useState<Media[]>([]);

  const { online } = useNetworkStatus();

  const {
    data: animeData,
    error,
    loading,
    refetch,
    fetchMore,
  } = useGetAnimes({
    variables: {
      page,
      perPage,
      ...(filterGenres?.length
        ? {
            genres: filterGenres,
          }
        : {}),
    },
    onCompleted(data) {
      setAllData((prev) => [...prev, ...(data?.Page?.media || [])]);
    },
    fetchPolicy: online ? "cache-and-network" : "cache-only",
  });

  const { ref, inView } = useInView({
    threshold: 0,
  });

  React.useEffect(() => {
    if (
      inView &&
      animeData &&
      animeData?.Page?.pageInfo?.hasNextPage &&
      !loading
    ) {
      setPage((prev) => prev + 1);

      fetchMore({
        variables: {
          page: page + 1,
        },
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [fetchMore, inView, page]);

  return (
    <section className="flex flex-col p-5">
      <div className="flex flex-1 justify-end">
        <GenreFilter
          value={filterGenres}
          setValue={setFilterGenres}
          onApply={() => {
            setPage(1);
            setAllData([]);
          }}
        />
      </div>
      <AnimeList
        isLoading={loading}
        error={!!error}
        data={allData}
        refetch={refetch}
      />
      {!loading && <div ref={ref} />}
    </section>
  );
}
