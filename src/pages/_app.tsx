import type { AppProps } from "next/app";
import "css/globals.css";
import { resetStyle } from "common/styles/common";
import { MantineProvider } from "@mantine/core";
import { ANIME_THEME } from "common/styles/mantine.theme";
import { DefaultLayout } from "layouts";

export default function App({ Component, pageProps }: AppProps) {
  return (
    <>
      <meta charSet="utf-8" />
      <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
      <meta
        name="viewport"
        content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no"
      />
      <meta name="description" content="Description" />
      <meta name="keywords" content="Keywords" />
      <title>Anime</title>

      <link rel="manifest" href="/manifest.json" />
      <link
        href="/icons/favicon-16x16.png"
        rel="icon"
        type="image/png"
        sizes="16x16"
      />
      <link
        href="/icons/favicon-32x32.png"
        rel="icon"
        type="image/png"
        sizes="32x32"
      />
      <link rel="apple-touch-icon" href="/apple-icon.png"></link>
      <meta name="theme-color" content="#317EFB" />
      <style global jsx>
        {resetStyle}
      </style>
      <MantineProvider theme={ANIME_THEME} withGlobalStyles>
        <DefaultLayout>
          <Component {...pageProps} />
        </DefaultLayout>
      </MantineProvider>
    </>
  );
}
