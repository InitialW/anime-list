import { render, screen, waitFor } from "@testing-library/react";
import {
  ApolloClient,
  ApolloProvider,
  HttpLink,
  InMemoryCache,
} from "@apollo/client";
import fetch from "cross-fetch";
import { Header } from "components/widgets";
import BookmarkPage from "pages/bookmark";

interface DefaultRenderProps {
  children: React.ReactNode;
}

jest.mock("next/router", () => require("next-router-mock"));

const intersectionObserverMock = () => ({
  observe: () => null,
  unobserve: () => null,
  disconnect: () => null,
});

window.IntersectionObserver = jest
  .fn()
  .mockImplementation(intersectionObserverMock);

const _client = new ApolloClient({
  uri: "https://graphql.anilist.co",
  cache: new InMemoryCache(),
  link: new HttpLink({ uri: "/graphql", fetch }),
  defaultOptions: {
    watchQuery: {
      fetchPolicy: "cache-and-network",
    },
  },
});

function DefaultRender(props: DefaultRenderProps) {
  return (
    <ApolloProvider client={_client}>
      <Header />
      {props.children}
    </ApolloProvider>
  );
}

describe("Bookmark Page", () => {
  it("renders header correctly", async () => {
    await waitFor(() =>
      render(
        <DefaultRender>
          <BookmarkPage />
        </DefaultRender>
      )
    );

    const link = screen.getByRole("link", {
      name: "Bookmark",
    });

    expect(link).toBeInTheDocument();
  });
});
