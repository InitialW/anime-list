import { render, screen, waitFor } from "@testing-library/react";
import HomePage from "pages";
import {
  ApolloClient,
  ApolloProvider,
  HttpLink,
  InMemoryCache,
} from "@apollo/client";
import fetch from "cross-fetch";
import { Header } from "components/widgets";

interface DefaultRenderProps {
  children: React.ReactNode;
}

jest.mock("next/router", () => require("next-router-mock"));

const intersectionObserverMock = () => ({
  observe: () => null,
  unobserve: () => null,
  disconnect: () => null,
});

window.IntersectionObserver = jest
  .fn()
  .mockImplementation(intersectionObserverMock);

const _client = new ApolloClient({
  uri: "https://graphql.anilist.co",
  cache: new InMemoryCache(),
  link: new HttpLink({ uri: "/graphql", fetch }),
  defaultOptions: {
    watchQuery: {
      fetchPolicy: "cache-and-network",
    },
  },
});

function DefaultRender(props: DefaultRenderProps) {
  return (
    <ApolloProvider client={_client}>
      <Header />
      {props.children}
    </ApolloProvider>
  );
}

describe("Home Page", () => {
  it("renders correctly", async () => {
    await waitFor(() =>
      render(
        <DefaultRender>
          <HomePage />
        </DefaultRender>
      )
    );

    const button = screen.getByRole("button", {
      name: /Filter Genre/i,
    });

    expect(button).toBeInTheDocument();
  });
});
