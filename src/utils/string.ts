import numeral from "numeral";

export function thousandSeparatorFormat(value: string | number): string {
  return numeral(`${value}`).format("0,0.[00]");
}

export function capitalizeWords(str?: string) {
  return (
    str?.replace(/(^\w{1})|(\s+\w{1})/g, (letter) => letter.toUpperCase()) ?? ""
  );
}
