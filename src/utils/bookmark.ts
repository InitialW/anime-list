import { Media } from "_graphql/types";

export function getBookmarkCache(): Media[] {
  if (typeof window === "undefined") return [];
  const bookmarks = localStorage.getItem("bookmarks");

  return bookmarks ? JSON.parse(bookmarks) : [];
}

export function updateBookmarkCache(item: Media) {
  if (typeof window === "undefined") return;

  const bookmarks = localStorage.getItem("bookmarks");

  let prevBookmarks: Media[] = bookmarks ? JSON.parse(bookmarks) : [];
  let newBookmarks;

  if (prevBookmarks?.find((bookmark) => bookmark.id === item.id)) {
    newBookmarks = prevBookmarks?.filter((bookmark) => bookmark.id !== item.id);
  } else {
    newBookmarks = prevBookmarks?.concat(item);
  }

  localStorage.setItem("bookmarks", JSON.stringify(newBookmarks));
}
