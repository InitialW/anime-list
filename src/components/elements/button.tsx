import cx from "classnames";

interface ButtonProps extends React.ButtonHTMLAttributes<HTMLButtonElement> {
  children?: React.ReactNode;
}

export default function Button(props: ButtonProps) {
  const { children, className, ...restProps } = props;

  const defaultStyles =
    "bg-primaryDefault rounded text-white c-white py-3 px-8 hover:scale-105";
  const _classNames = cx(defaultStyles, className);

  return (
    <button className={_classNames} {...restProps}>
      {children}
    </button>
  );
}
