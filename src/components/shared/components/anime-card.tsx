import { Media } from "_graphql/types";
import * as AspectRatio from "@radix-ui/react-aspect-ratio";
import { thousandSeparatorFormat } from "utils/string";

interface Props extends Media {
  onClick: () => void;
}

export default function AnimeCard(props: Props) {
  const { coverImage, title, genres, popularity, onClick } = props;

  return (
    <div
      onClick={() => onClick()}
      style={{
        boxShadow: `rgba(0, 0, 0, 0.05) 0px 6px 24px 0px, rgba(0, 0, 0, 0.08) 0px 0px 0px 1px`,
      }}
      className="flex flex-col rounded mb-6 lg:w-five w-three cursor-pointer hover:scale-105 overflow-hidden"
    >
      <AspectRatio.AspectRatio ratio={1.05}>
        <img className="w-full h-full" alt="anime-img" src={coverImage.large} />
      </AspectRatio.AspectRatio>
      <div className="p-4">
        <p className="mb-2 typo-body-1-semibold">{`${
          title?.english || "No Title Provided"
        }`}</p>
        <p className="mb-2">{`${
          genres?.map((_genre) => _genre)?.join(", ") || "No Genre Specified"
        }`}</p>
        <p className="typo-body-3-regular">{`Popularity: ${
          thousandSeparatorFormat(popularity) || "0"
        }`}</p>
      </div>
    </div>
  );
}
