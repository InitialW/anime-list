import Link from "next/link";
import * as React from "react";
import cx from "classnames";
import { useRouter } from "next/router";
import { useNetworkStatus } from "common/containers";

const Nav = [
  {
    title: "Home",
    path: "/",
  },
  {
    title: "Bookmark",
    path: "/bookmark",
  },
];

export default function Header() {
  const router = useRouter();

  const { online } = useNetworkStatus();

  return (
    <header
      style={{
        height: 80,
        boxShadow: `rgba(0, 0, 0, 0.16) 0px 1px 4px`,
      }}
      className="px-16 py-4 fixed left-0 right-0 z-50 bg-white"
    >
      <div className="flex justify-end items-center h-full">
        <div className="flex justify-center items-center">
          <div
            className={cx(
              "mr-2 w-4 h-4 rounded-full",
              online ? "bg-greenDefault" : "bg-redDefault"
            )}
          />
          <span>{online ? "Online" : "Offline"}</span>
        </div>
        {Nav?.map((_nav) => (
          <Link key={_nav.path} href={_nav.path}>
            <span
              className={cx(
                "pl-8 hover:text-blueDefault",
                _nav.path === router.pathname ? "text-blueDefault" : ""
              )}
            >
              {_nav.title}
            </span>
          </Link>
        ))}
      </div>
    </header>
  );
}
