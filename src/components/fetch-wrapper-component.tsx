import React from "react";
import Button from "./elements/button";

interface WrapperProps {
  isLoading?: boolean;
  error?: boolean | null;
  loadingComponent?: React.ReactNode;
  errorComponent?: React.ReactNode;
  component: React.ReactNode;
  onRetry?: () => void;
  noData?: boolean;
  emptyText?: string;
}

export default function FetchWrapperComponent(props: WrapperProps) {
  const {
    isLoading = false,
    error,
    onRetry,
    loadingComponent,
    component,
    errorComponent,
    noData,
    emptyText,
  } = props;

  if (isLoading) {
    return <>{loadingComponent}</> || null;
  } else if (error) {
    if (errorComponent) {
      return <>{errorComponent}</>;
    }

    return (
      <Button className="w-60" onClick={onRetry}>
        Error!, Tap To Retry
      </Button>
    );
  }

  if (noData) {
    return <p className="w-full text-center">{emptyText}</p>;
  }

  return <>{component}</>;
}
