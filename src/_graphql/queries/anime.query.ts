import { QueryHookOptions, gql, useQuery } from "@apollo/client";
import {
  GetAnimeInput,
  GetAnimesInput,
  MediaResult,
  Page,
  PageResult,
} from "_graphql/types";

export const GET_ANIMES = gql`
  query getAnimes(
    $page: Int
    $perPage: Int
    $search: String
    $genres: [String]
  ) {
    Page(page: $page, perPage: $perPage) {
      media(
        isAdult: false
        sort: POPULARITY_DESC
        type: ANIME
        search: $search
        genre_in: $genres
      ) {
        id
        title {
          english
        }
        coverImage {
          large
          extraLarge
        }
        popularity
        genres
        description
        averageScore
      }
      pageInfo {
        currentPage
        hasNextPage
        total
      }
    }
  }
`;

export const GET_ANIME = gql`
  query getAnime($id: Int) {
    Media(id: $id) {
      id
      title {
        english
      }
      coverImage {
        large
        extraLarge
      }
      genres
      popularity
      description
      averageScore
    }
  }
`;

export function useGetAnimes(
  options?: QueryHookOptions<PageResult, GetAnimesInput>
) {
  return useQuery<PageResult, GetAnimesInput>(GET_ANIMES, options);
}

export function useGetAnime(
  options?: QueryHookOptions<MediaResult, GetAnimeInput>
) {
  return useQuery<MediaResult, GetAnimeInput>(GET_ANIME, options);
}
