import gql from 'graphql-tag';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  Any: any;
};


export type Media = {
  __typename?: 'Media';
  id: Scalars['String'];
  title: MediaTitle;
  coverImage: MediaCoverImage;
  popularity: Scalars['Int'];
  genres?: Maybe<Array<Scalars['String']>>;
  averageScore: Scalars['Int'];
  description: Scalars['String'];
};

export type MediaCoverImage = {
  __typename?: 'MediaCoverImage';
  large: Scalars['String'];
  extraLarge: Scalars['String'];
};

export type MediaResult = {
  __typename?: 'MediaResult';
  Media: Media;
};

export type MediaTitle = {
  __typename?: 'MediaTitle';
  english: Scalars['String'];
};

export enum MediaType {
  Anime = 'ANIME',
  Manga = 'MANGA'
}

export type Page = {
  __typename?: 'Page';
  pageInfo: PageInfo;
  media?: Maybe<Array<Media>>;
};

export type PageInfo = {
  __typename?: 'PageInfo';
  total: Scalars['Int'];
  currentPage: Scalars['Int'];
  lastPage: Scalars['Int'];
  hasNextPage?: Maybe<Scalars['Boolean']>;
  perPage: Scalars['Int'];
};

export type PageResult = {
  __typename?: 'PageResult';
  Page: Page;
};

export enum Sort {
  ScoreDesc = 'SCORE_DESC',
  PopularityDesc = 'POPULARITY_DESC',
  TrendingDesc = 'TRENDING_DESC'
}

export type GetAnimeInput = {
  id: Scalars['Int'];
};

export type GetAnimesInput = {
  page?: Maybe<Scalars['Int']>;
  perPage?: Maybe<Scalars['Int']>;
  sort?: Maybe<Array<Maybe<Sort>>>;
  genres?: Maybe<Array<Maybe<Scalars['String']>>>;
};
